"use strict";

var fs = require("fs");
var pathjs = require("path");
var moment = require('moment-timezone');
var SortedArray = require("collections/sorted-array");

// var outputDataPath = './holiday/dailyLogged/withWeekendsWeight';
// var outputDataPath = './holiday/interpolationReady/withWeekendsWeight';
var outputDataPath = './holiday/interpolationReady/binaryHourly';
var logFileNameFormat = "YYYYMMDD[.log]";


function Easter(Y) {
    var C = Math.floor(Y/100);
    var N = Y - 19*Math.floor(Y/19);
    var K = Math.floor((C - 17)/25);
    var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
    I = I - 30*Math.floor((I/30));
    I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
    var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
    J = J - 7*Math.floor(J/7);
    var L = I - J;
    var M = 3 + Math.floor((L + 40)/44);
    var D = L + 28 - 31*Math.floor(M/4);

    return new moment.tz([Y, M-1, D], 'America/Sao_Paulo');
}

var holidays = [
    {"date": "1/1", "value": 1, "name": "Ano novo", "id": 1},
    {"date": "1/25", "value": 1, "name": "Aniversario de Sao Paulo", "id": 2},
    {"date": function(Y){return Easter(Y).subtract(48, 'days');} , "value": 0.5, "name": "Segunda de Carnaval", "id": 3},
    {"date": function(Y){return Easter(Y).subtract(47, 'days');} , "value": 1, "name": "Terca de Carnaval", "id": 3},
    {"date": function(Y){return Easter(Y).subtract(46, 'days');} , "value": 0.5, "name": "Quarta de Cinzas", "id": 3},
    {"date": function(Y){return Easter(Y).subtract(2, 'days');} , "value": 1, "name": "Sexta Santa", "id": 4},
    {"date": function(Y){return Easter(Y);} , "value": 1, "name": "Pascoa", "id": 4},
    {"date": "4/21", "value": 1, "name": "Tiradentes", "id": 5},
    {"date": "5/1", "value": 1, "name": "Dia do trabalho", "id": 6},
    {"date": function(Y){return Easter(Y).add(60, 'days');} , "value": 1, "name": "Corpus Christi", "id": 7},
    {"date": "7/9", "value": 1, "name": "Revolucao constitucional 1932", "id": 8},
    {"date": "10/12", "value": 1, "name": "Dia de Nossa Sra Aparecida", "id": 9},
    {"date": "11/2", "value": 1, "name": "Finados", "id": 10},
    {"date": "11/15", "value": 1, "name": "Proclamacao da Republica", "id": 11},
    {"date": "11/20", "value": 1, "name": "Consiencia Negra", "id": 12},
    {"date": "12/24", "value": 0.5, "name": "Vespera de Natal", "id": 13},
    {"date": "12/25", "value": 1, "name": "Natal", "id": 13},
    {"date": "12/31", "value": 0.5, "name": "Vespera de Ano Novo", "id": 1},
];

function binaryHolidaysInterpolationReadyDaily(arrMoment)
{
    var outputDataPath = './holiday/interpolationReady/binaryDaily';
    var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    var comment = "feriado:\%f";
    for(var i=0; i<arrMoment.length; i++) {
        var zeroBefore = arrMoment[i].clone().subtract(1, "day");
        var zeroAfter = arrMoment[i].clone().add(1, "day");
        if(i <= 0 || zeroBefore.valueOf() !== arrMoment[i-1].valueOf()) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroBefore.clone().utc().format(logFileNameFormat)), zeroBefore, classPath, [0, 0], comment);
        }
        writeToDatastoreLog(pathjs.join(outputDataPath, arrMoment[i].clone().utc().format(logFileNameFormat)), arrMoment[i], classPath,[arrMoment[i].holidayValue, arrMoment[i].holidayId], comment);
        if(i+1 >= arrMoment.length || zeroAfter.valueOf() !== arrMoment[i+1].valueOf()) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroAfter.clone().utc().format(logFileNameFormat)), zeroAfter, classPath, [0, 0], comment);
        }
    }
}

function binaryHolidaysInterpolationReadyHourly(arrMoment)
{
    var outputDataPath = './holiday/interpolationReady/binaryHourly';
    var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    var comment = "feriado:\%f";
    for(var i=0; i<arrMoment.length; i++) {
        var zeroBefore = arrMoment[i].clone().subtract(1, "hour");
        var zeroAfter = arrMoment[i].clone().add(24, "hours");
        if(i <= 0 || Math.abs(zeroBefore.diff(arrMoment[i-1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroBefore.clone().utc().format(logFileNameFormat)), zeroBefore, classPath, [0, 0], comment);
        }
        var tempDay = arrMoment[i].clone();
        tempDay.holidayValue = arrMoment[i].holidayValue;
        tempDay.holidayId = arrMoment[i].holidayId;
        for(var h=0; h<24; h++) {
            tempDay.hour(h);
            writeToDatastoreLog(pathjs.join(outputDataPath, tempDay.clone().utc().format(logFileNameFormat)), tempDay, classPath,[tempDay.holidayValue, tempDay.holidayId], comment);
        }
        if(i+1 >= arrMoment.length || Math.abs(zeroAfter.diff(arrMoment[i+1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroAfter.clone().utc().format(logFileNameFormat)), zeroAfter, classPath, [0, 0], comment);
        }
    }
}

function smoothHolidaysInterpolationReadyHourly(arrMoment)
{
    var outputDataPath = './holiday/interpolationReady/smoothHourly';
    var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    var comment = "feriado:\%f";
    var lastHolidayValue = 0, nextHolidayValue = 0;
    for(var i=0; i<arrMoment.length; i++) {
        var zeroBefore = arrMoment[i].clone().subtract(1, "hour");
        var zeroAfter = arrMoment[i].clone().add(24, "hours");
        if(i <= 0 || Math.abs(zeroBefore.diff(arrMoment[i-1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroBefore.clone().utc().format(logFileNameFormat)), zeroBefore, classPath, [0, 0], comment);
            lastHolidayValue = 0;
        }
        var tempDay = arrMoment[i].clone();
        tempDay.holidayValue = arrMoment[i].holidayValue;
        tempDay.holidayId = arrMoment[i].holidayId;
        for(var h=0; h<24; h++) {
            tempDay.hour(h);
            // var y = h-11.5;
            // var y4 = Math.pow(y, 4);//y*y*y*y;
            // // 11.5 ^ 4 = 17490.0625
            // var x = Math.min((-y4 + 17490.0625)/17490.0625, 1);
            if(i+1 < arrMoment.length && arrMoment[i+1].diff(arrMoment[i], "days") <= 1) {
                nextHolidayValue = arrMoment[i+1].holidayValue;
            } else {
                nextHolidayValue = 0;
            }
            // writeToDatastoreLog(pathjs.join(outputDataPath, tempDay.clone().utc().format(logFileNameFormat)), tempDay, classPath,[tempDay.holidayValue * x], comment);
            var x = tempDay.holidayValue = (h < 6 ? cosineInterpolate(lastHolidayValue, arrMoment[i].holidayValue, h/5.0) : (h < 18 ? arrMoment[i].holidayValue : cosineInterpolate(arrMoment[i].holidayValue, nextHolidayValue, (h-18)/5.0)));
            writeToDatastoreLog(pathjs.join(outputDataPath, tempDay.clone().utc().format(logFileNameFormat)), tempDay, classPath,[x, tempDay.holidayId], comment);
        }
        lastHolidayValue = tempDay.holidayValue;
        // lastHolidayValue = arrMoment[i].holidayValue;
        if(i+1 >= arrMoment.length || Math.abs(zeroAfter.diff(arrMoment[i+1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroAfter.clone().utc().format(logFileNameFormat)), zeroAfter, classPath, [0, 0], comment);

        }
    }
}



function binaryExtendedHolidaysInterpolationReadyDaily(arrMoment)
{
    var outputDataPath = './holidayExtended/interpolationReady/binaryDaily';
    var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    var comment = "feriado:\%f prolongado:\%f";
    var momentDates = new SortedArray(arrMoment);
    var index, tempDay;
    for(var i=0; i<arrMoment.length; i++) {
       if(arrMoment[i].day() === 1) { //Monday
            // console.log("Monday");
            var weekendBefore = arrMoment[i].clone().subtract(2, "days");
            while(weekendBefore.valueOf() < arrMoment[i].valueOf()) {
                index = momentDates.indexOf(weekendBefore);
                if(index < 0) {
                    tempDay = weekendBefore.clone();
                    tempDay.holidayValue = 0;
                    tempDay.extendedHoliday = 1;
                    tempDay.holidayId = arrMoment[i].holidayId;
                    momentDates.add(tempDay);
                }
                else {
                    arrMoment[index].extendedHoliday = 1;
                }
                weekendBefore.add(1, "day");
            }
            arrMoment[i].extendedHoliday = 1;
        }
        else if(arrMoment[i].day() === 5) { //Friday
            // console.log("Friday");
            var weekendAfter = arrMoment[i].clone().add(2, "day");
            while(weekendAfter.valueOf() > arrMoment[i].valueOf()) {
                index = momentDates.indexOf(weekendAfter);
                if(index < 0) {
                    tempDay = weekendAfter.clone();
                    tempDay.holidayValue = 0;
                    tempDay.extendedHoliday = 1;
                    tempDay.holidayId = arrMoment[i].holidayId;
                    momentDates.add(tempDay);
                }
                else {
                    momentDates.array[index].extendedHoliday = 1;
                }
                weekendAfter.subtract(1, "day");
            }
            arrMoment[i].extendedHoliday = 1;
        }
        else if(i+1 < arrMoment.length)
        {
            // console.log("Holidays next to each other");
            var daysToNextHoliday = arrMoment[i+1].diff(arrMoment[i], "days");
            if(daysToNextHoliday <= 1) {
                // var index = momentDates.indexOf(arrMoment[i]);
                // momentDates.array[index].extendedHoliday = 1;
                // index = momentDates.indexOf(arrMoment[i+1]);
                // momentDates.array[index].extendedHoliday = 1;
               arrMoment[i].extendedHoliday = arrMoment[i+1].extendedHoliday = 1;
            }
        }
    }
    arrMoment = momentDates.array;
    for(i=0; i<arrMoment.length; i++) {
        arrMoment[i].extendedHoliday = arrMoment[i].extendedHoliday || 0;
        var zeroBefore = arrMoment[i].clone().subtract(1, "day");
        var zeroAfter = arrMoment[i].clone().add(1, "day");

        if(i <= 0 || zeroBefore.valueOf() !== arrMoment[i-1].valueOf()) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroBefore.clone().utc().format(logFileNameFormat)), zeroBefore, classPath, [0, 0, 0], comment);
        }

        writeToDatastoreLog(pathjs.join(outputDataPath, arrMoment[i].clone().utc().format(logFileNameFormat)), arrMoment[i],
                             classPath, [arrMoment[i].holidayValue, arrMoment[i].extendedHoliday, arrMoment[i].holidayId], comment);

        if(i+1 >= arrMoment.length || zeroAfter.valueOf() !== arrMoment[i+1].valueOf()) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroAfter.clone().utc().format(logFileNameFormat)), zeroAfter, classPath, [0, 0, 0], comment);
        }
    }
}

function binaryExtendedHolidaysInterpolationReadyHourly(arrMoment)
{
    var outputDataPath = './holidayExtended/interpolationReady/binaryHourly';
    var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    var comment = "feriado:\%f prolongado:\%f";
    var momentDates = new SortedArray(arrMoment);
    var index, tempDay;

    for(var i=0; i<arrMoment.length; i++) {
       if(arrMoment[i].day() === 1) { //Monday
            // console.log("Monday");
            var weekendBefore = arrMoment[i].clone().subtract(2, "days");
            while(weekendBefore.valueOf() < arrMoment[i].valueOf()) {
                index = momentDates.indexOf(weekendBefore);
                if(index < 0) {
                    tempDay = weekendBefore.clone();
                    tempDay.holidayValue = 0;
                    tempDay.extendedHoliday = 1;
                    tempDay.holidayId = arrMoment[i].holidayId;
                    momentDates.add(tempDay);
                }
                else {
                    arrMoment[index].extendedHoliday = 1;
                }
                weekendBefore.add(1, "day");
            }
            arrMoment[i].extendedHoliday = 1;
        }
        else if(arrMoment[i].day() === 5) { //Friday
            // console.log("Friday");
            var weekendAfter = arrMoment[i].clone().add(2, "day");
            while(weekendAfter.valueOf() > arrMoment[i].valueOf()) {
                index = momentDates.indexOf(weekendAfter);
                if(index < 0) {
                    tempDay = weekendAfter.clone();
                    tempDay.holidayValue = 0;
                    tempDay.extendedHoliday = 1;
                    tempDay.holidayId = arrMoment[i].holidayId;
                    momentDates.add(tempDay);
                }
                else {
                    momentDates.array[index].extendedHoliday = 1;
                }
                weekendAfter.subtract(1, "day");
            }
            arrMoment[i].extendedHoliday = 1;
        }
        else if(i+1 < arrMoment.length)
        {
            // console.log("Holidays next to each other");
            var daysToNextHoliday = arrMoment[i+1].diff(arrMoment[i], "days");
            if(daysToNextHoliday <= 1) {
                // var index = momentDates.indexOf(arrMoment[i]);
                // momentDates.array[index].extendedHoliday = 1;
                // index = momentDates.indexOf(arrMoment[i+1]);
                // momentDates.array[index].extendedHoliday = 1;
               arrMoment[i].extendedHoliday = arrMoment[i+1].extendedHoliday = 1;
            }
        }
    }
    arrMoment = momentDates.array;
    for(i=0; i<arrMoment.length; i++) {
        arrMoment[i].extendedHoliday = arrMoment[i].extendedHoliday || 0;
        var zeroBefore = arrMoment[i].clone().subtract(1, "hour");
        var zeroAfter = arrMoment[i].clone().add(24, "hours");
        if(i <= 0 || Math.abs(zeroBefore.diff(arrMoment[i-1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroBefore.clone().utc().format(logFileNameFormat)), zeroBefore, classPath, [0, 0, 0], comment);
        }
        tempDay = arrMoment[i].clone();
        tempDay.holidayValue = arrMoment[i].holidayValue;
        tempDay.extendedHoliday = arrMoment[i].extendedHoliday;
        tempDay.holidayId = arrMoment[i].holidayId;
        for(var h=0; h<24; h++) {
            tempDay.hour(h);
            writeToDatastoreLog(pathjs.join(outputDataPath, tempDay.clone().utc().format(logFileNameFormat)), tempDay, classPath,[tempDay.holidayValue, tempDay.extendedHoliday, tempDay.holidayId], comment);
        }
        if(i+1 >= arrMoment.length || Math.abs(zeroAfter.diff(arrMoment[i+1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroAfter.clone().utc().format(logFileNameFormat)), zeroAfter, classPath, [0, 0, 0], comment);
        }
    }
}

function smoothExtendedHolidaysInterpolationReadyHourly(arrMoment)
{
    var outputDataPath = './holidayExtended/interpolationReady/smoothHourly';
    var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    var comment = "feriado:\%f prolongado:\%f";
    var momentDates = new SortedArray(arrMoment);
    var lastHolidayId = 0;
    var index, tempDay;

    for(var i=0; i<arrMoment.length; i++) {
        lastHolidayId = arrMoment[i].holidayId;
        if(arrMoment[i].day() === 1) { //Monday
            // console.log("Monday");
            var weekendBefore = arrMoment[i].clone().subtract(2, "days");
            while(weekendBefore.valueOf() < arrMoment[i].valueOf()) {
                index = momentDates.indexOf(weekendBefore);
                if(index < 0) {
                    tempDay = weekendBefore.clone();
                    tempDay.holidayValue = 0;
                    tempDay.extendedHoliday = 1;
                    tempDay.holidayId = lastHolidayId;
                    momentDates.add(tempDay);
                }
                else {
                    arrMoment[index].extendedHoliday = 1;
                }
                weekendBefore.add(1, "day");
            }
            arrMoment[i].extendedHoliday = 1;
        }
        else if(arrMoment[i].day() === 5) { //Friday
            // console.log("Friday");
            var weekendAfter = arrMoment[i].clone().add(2, "day");
            while(weekendAfter.valueOf() > arrMoment[i].valueOf()) {
                index = momentDates.indexOf(weekendAfter);
                if(index < 0) {
                    tempDay = weekendAfter.clone();
                    tempDay.holidayValue = 0;
                    tempDay.extendedHoliday = 1;
                    tempDay.holidayId = lastHolidayId;
                    momentDates.add(tempDay);
                }
                else {
                    momentDates.array[index].extendedHoliday = 1;
                }
                weekendAfter.subtract(1, "day");
            }
            arrMoment[i].extendedHoliday = 1;
        }
        else if(i+1 < arrMoment.length)
        {
            // console.log("Holidays next to each other");
            var daysToNextHoliday = arrMoment[i+1].diff(arrMoment[i], "days");
            if(daysToNextHoliday <= 1) {
                // var index = momentDates.indexOf(arrMoment[i]);
                // momentDates.array[index].extendedHoliday = 1;
                // index = momentDates.indexOf(arrMoment[i+1]);
                // momentDates.array[index].extendedHoliday = 1;
               arrMoment[i].extendedHoliday = arrMoment[i+1].extendedHoliday = 1;
            }
        }
    }
    arrMoment = momentDates.array;
    var lastHolidayValue = 0, nextHolidayValue = 0;
    var lastExtendedHolidayValue = 0, nextExtendedHolidayValue = 0;
    for(i=0; i<arrMoment.length; i++) {
        arrMoment[i].extendedHoliday = arrMoment[i].extendedHoliday || 0;
        var zeroBefore = arrMoment[i].clone().subtract(1, "hour");
        var zeroAfter = arrMoment[i].clone().add(24, "hours");
        if(i <= 0 || Math.abs(zeroBefore.diff(arrMoment[i-1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroBefore.clone().utc().format(logFileNameFormat)), zeroBefore, classPath, [0,0,0], comment);
            lastHolidayValue = 0;
            lastExtendedHolidayValue = 0;
        }
        tempDay = arrMoment[i].clone();
        tempDay.holidayValue = arrMoment[i].holidayValue;
        tempDay.extendedHoliday = arrMoment[i].extendedHoliday;
        tempDay.holidayId = arrMoment[i].holidayId;
        for(var h=0; h<24; h++) {
            tempDay.hour(h);
            if(i+1 < arrMoment.length && arrMoment[i+1].diff(arrMoment[i], "days") <= 1) {
                nextHolidayValue = arrMoment[i+1].holidayValue;
                nextExtendedHolidayValue = arrMoment[i+1].extendedHoliday;
            } else {
                nextHolidayValue = 0;
                nextExtendedHolidayValue = 0;
            }
            // writeToDatastoreLog(pathjs.join(outputDataPath, tempDay.clone().utc().format(logFileNameFormat)), tempDay, classPath,[tempDay.holidayValue * x], comment);
            var x = tempDay.holidayValue = (h < 6 ? cosineInterpolate(lastHolidayValue, arrMoment[i].holidayValue, h/5.0) : (h < 18 ? arrMoment[i].holidayValue : cosineInterpolate(arrMoment[i].holidayValue, nextHolidayValue, (h-18)/5.0)));
            var y = tempDay.extendedHoliday = (h < 6 ? cosineInterpolate(lastExtendedHolidayValue, arrMoment[i].extendedHoliday, h/5.0) : (h < 18 ? arrMoment[i].extendedHoliday : cosineInterpolate(arrMoment[i].extendedHoliday, nextExtendedHolidayValue, (h-18)/5.0)));
            writeToDatastoreLog(pathjs.join(outputDataPath, tempDay.clone().utc().format(logFileNameFormat)), tempDay, classPath,[x, y, tempDay.holidayId], comment);
        }
        lastHolidayValue = tempDay.holidayValue;
        lastExtendedHolidayValue = tempDay.extendedHoliday;
        if(i+1 >= arrMoment.length || Math.abs(zeroAfter.diff(arrMoment[i+1], "hours")) > 24) {
            writeToDatastoreLog(pathjs.join(outputDataPath, zeroAfter.clone().utc().format(logFileNameFormat)), zeroAfter, classPath, [0,0,0], comment);
            nextHolidayValue = 0;
            nextExtendedHolidayValue = 0;
        }
    }
}

(function main () {
    // --------------- Output Folder Structure ------------------
    // holiday/
    // |── interpolationReady/
    // |   |── dailyLogged/
    // |   |    |── withWeekendsWeight/
    // |   |    |   |── YYYYMMDD.log
    // ----------------------------------------------------------
    var startingYear = 2011;
    var endingYear = 2016;
    // var endingYear = 2011;
    makeDir(outputDataPath);
    var momentDates = new SortedArray();
    // var classPath = pathjs.relative("./", outputDataPath).replace(/\//g, ".");
    // var comment = "feriado:\%f prolongado:\%f";
    // var logFileNameFormat = "YYYYMMDD[.log]";
    var holidayFiller = function (holiday) {
                var y = this;
                var date = typeof(holiday.date) === "function" ? holiday.date(y) : new moment.tz(holiday.date, "MM/DD",'America/Sao_Paulo').year(y);
                date.holidayValue = holiday.value;
                date.holidayId = holiday.id;
                momentDates.add(date);
            };
    for(var y = startingYear; y <= endingYear; y++)
     // var y = 2011;
    {
        holidays.forEach(holidayFiller, y);
            // var holiday = holidays[0, 0];
            // var date = typeof(holiday.date) === "function" ? holiday.date(y) : new moment.tz(holiday.date, "MM/DD",'America/Sao_Paulo').year(y);
            // date.holidayValue = holiday.value;
            // momentDates.push(date)
    }
    // // fs.writeFileSync("holidays.txt", "");
    binaryHolidaysInterpolationReadyDaily(momentDates.array);
    binaryHolidaysInterpolationReadyHourly(momentDates.array);
    smoothHolidaysInterpolationReadyHourly(momentDates.array);
    binaryExtendedHolidaysInterpolationReadyDaily(momentDates.array);
    smoothExtendedHolidaysInterpolationReadyHourly(momentDates.array);
    // binaryExtendedHolidaysInterpolationReadyHourly(momentDates.array);


    // for(var i=0; i<momentDates.length; i++) {
    //     var tempDay = arrMoment[i];
    //     console.log("tempDay: " + tempDay.format() + " i:" + i);
    //     tempDay.longHoliday = tempDay.longHoliday || 0;
    //     if(i+1 < momentDates.length) {
    //         var daysToNextHoliday = arrMoment[i+1].diff(tempDay, "days");
    //         if(daysToNextHoliday <= 1) {
    //             console.log("daysToNextHoliday: " + daysToNextHoliday);
    //             arrMoment[i+1].longHoliday = tempDay.longHoliday = 1;
    //         }
    //         else if(tempDay.day() === 6 && daysToNextHoliday <= 2) {
    //             console.log("is saturnday daysToNextHoliday: " + daysToNextHoliday);
    //             arrMoment[i+1].longHoliday = tempDay.longHoliday = 1;
    //         }
    //         else if(tempDay.day() === 1) {
    //             console.log("it's Monday!");
    //             var weekendCount = 2;
    //             tempDay.longHoliday = 1;
    //             var weekend = tempDay.clone();
    //             weekend.subtract(weekendCount, "days");
    //             for(var j=0; j<weekendCount; j++){
    //                 if(i<=0 || arrMoment[i-1].diff(weekend, "days") === 0) {
    //                     weekend.holidayValue = 0;
    //                     weekend.longHoliday = 1;
    //                     writeToDatastoreLog("holidays.txt", weekend.valueOf(), classPath,
    //                                                      [weekend.holidayValue, weekend.longHoliday], comment);
    //                 }
    //                 weekend.add(1, "day");
    //             }
    //         }
    //         else if(tempDay.day() === 5) {
    //             console.log("it's Friday!");
    //             var weekendCount = 2;
    //             tempDay.longHoliday = 1;
    //             var weekend = tempDay.clone();
    //             for(var j=0; j<weekendCount; j++){
    //                 weekend.add(1, "day");
    //                 if(i<momentDates.length && arrMoment[i+1].diff(weekend, "days") === 0) {
    //                     weekend.holidayValue = 0;
    //                     weekend.longHoliday = 1;
    //                     writeToDatastoreLog("holidays.txt", weekend.valueOf(), classPath,
    //                                                      [weekend.holidayValue, weekend.longHoliday], comment);
    //                 }
    //             }
    //         }
    //         else {
    //             console.log("It isn't Monday or Friday.");
    //             var zeroDay = tempDay.clone().add(1, "day");
    //             zeroDay.longHoliday = zeroDay.holidayValue = 0;
    //             writeToDatastoreLog("holidays.txt", zeroDay.valueOf(), classPath,
    //                                              [zeroDay.holidayValue, zeroDay.longHoliday], comment);
    //         }
    //     }
    //     if(!tempDay.longHoliday) {
    //         console.log("It isn't a long holiday.");
    //         var zeroDay = tempDay.clone().subtract(1, "day");
    //         zeroDay.longHoliday = zeroDay.holidayValue = 0;
    //         writeToDatastoreLog("holidays.txt", zeroDay.valueOf(), classPath,
    //                                          [zeroDay.holidayValue, zeroDay.longHoliday], comment);
    //     }
    //     writeToDatastoreLog("holidays.txt", tempDay.valueOf(), classPath,
    //                                      [tempDay.holidayValue, tempDay.longHoliday], comment);

    // }
    // momentDates.forEach(function(date) {
    //     fs.appendFileSync("holidays.txt", date.format() + "\r\n");
    // });
})();

function writeToDatastoreLog(filename, datetime, tagname, data_array, str_commentary) {
    if(datetime && tagname && data_array) {
        var unix_ms = 0;
        if(datetime instanceof moment) {
            filename = filename || datetime.clone().utc().format(logFileNameFormat);
            unix_ms = datetime.valueOf();
        }
        else if(datetime instanceof Date) {
            datetime = new moment(datetime);
            filename = filename || datetime.clone().utc().format(logFileNameFormat);
            unix_ms = datetime.valueOf();
        }
        else {
            datetime = new moment(new Date(datetime));
            filename = filename || datetime.clone().utc().format(logFileNameFormat);
            unix_ms = datetime.valueOf();
        }
        // console.log(datetime.format() + " | weekday: " + datetime.day() + " | holidayValue: " + data_array[0, 0] + " | extendedHoliday:" + data_array[1] + " | path:" + filename);
        console.log(datetime.format() + " | weekday: " + datetime.day() + " | holidayValue: " + data_array[0] + " | extendedHoliday:" + data_array[1]);
        var dir = pathjs.dirname(filename);
        makeDir(dir);
        var data_str = [];
        data_array.forEach(function(item){
            // var n = 0;
            // data_str.push(typeof(item)==="number"?item:((n=parseFloat(item))?n:0));
            // console.log(typeof(item), item);
            data_str.push(Number(item)||0);
        });
        var dataLog = `${unix_ms}, 2, ${data_array.length}, LOG, ${tagname}, ${data_str.join()}, ${str_commentary} \r\n`;
        fs.appendFileSync(filename, dataLog);
    } else {
        throw new Error("The parameter " + (datetime ? (tagname ? "data_array" : "tagname") : "datetime") + " is missing");
    }
}

// @params:
// y1 origin value
// y2 target value
// mu 0...1 the interpolation progression
function cosineInterpolate(y1, y2, mu)
{
   var mu2 = (1-Math.cos(mu*Math.PI))/2;
   return(y1*(1-mu2)+y2*mu2);
}

function makeDir(str_path) {
    try {
        fs.mkdirSync(str_path);
    } catch(ex) {
        // console.log(JSON.stringify(ex));
        if(ex.code !== "EEXIST") {
            if(ex.code === "ENOENT") {
                var dirPathSplit = str_path.split(pathjs.sep);
                var dirPath = "";
                dirPathSplit.forEach(function (value) {
                    dirPath = pathjs.join(dirPath, value);
                    makeDir(dirPath);
                });
            } else {
                console.log(JSON.stringify(ex));
                throw ex;
            }
        }
    }
}


/*            if(date.day() < 3) {
                date.longHoliday = 1;
                var tempDay = date.clone().day(-2); //last Friday
                tempDay.holidayValue = 0;
                tempDay.longHoliday = 0;
                writeToDatastoreLog(tempDay.clone().utc().format(logFileNameFormat), tempDay.valueOf(), classPath,
                                                         [tempDay.holidayValue, tempDay.longHoliday], comment);
                tempDay.add(1, "days");
                while(tempDay < date) {
                    tempDay.longHoliday = tempDay.day() === 1 ? 0.5 : 1;
                    writeToDatastoreLog(tempDay.clone().utc().format(logFileNameFormat), tempDay.valueOf(), classPath,
                                                             [tempDay.holidayValue, tempDay.longHoliday], comment);
                    tempDay.add(1, "days");
                }
                tempDay.longHoliday = 0;
                tempDay.add(1, "days");
                momentDates.push(tempDay.clone());
            }
            else if(date.day() > 3) {
                date.longHoliday = 1;
                var tempDay = date.clone().day(8); //next Monday
                tempDay.holidayValue = 0;
                tempDay.longHoliday = 0;
                momentDates.push(tempDay);
                tempDay = tempDay.clone();
                tempDay.value = 0;
                tempDay.add(1, "days");
                while(tempDay < date) {
                    tempDay.longHoliday = tempDay.day() === 1 ? 0.5 : 1;
                    momentDates.push(tempDay.clone());
                    tempDay.add(1, "days");
                }
                tempDay.longHoliday = 0;
                tempDay.add(1, "days");
                momentDates.push(tempDay.clone());
            }
            else {
                var tempDay = date.clone();
                tempDay.subtract(1, "d");
                tempDay.holidayValue = 0;
                tempDay.longHoliday = 0;
                momentDates.push(tempDay.clone());
                tempDay.add(2, "d");
                momentDates.push(tempDay.clone());
            }*/